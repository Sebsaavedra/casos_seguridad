# -*- coding: utf-8 -*-
from odoo import models, fields, api


class casos_seguridad(models.Model):
    _name = 'seguridad.seguridad'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin']
    _description = "Casos Seguridad"



    fecha_caso = fields.Date(string="Fecha Caso", track_visibility='onchange', index=True)

    asunto = fields.Char(string="Asunto",track_visibility='onchange', index=True)

    numero_disco = fields.Selection([('1','Disco 1'),
                                         ('2','Disco 2'),
                                         ('3','Disco 3'),
                                         ('4','Disco 4'),
                                         ('5','Disco 5'),
                                         ('6','Disco 6'),
                                         ('7','Disco 7'),
                                         ('8','Disco 8'),
                                         ('9','Disco 9'),
                                         ('10','Disco 10')], track_visibility='onchange', index=True)
    ruta_carpeta = fields.Char(string="Ruta Carpeta",track_visibility='onchange', index=True)



    # establecimiento = fields.Selection([('1','CEDIAM'),
    #                                     ('2','Centro de Rehabilitación Integral Comunitario'),
    #                                     ('3','Centro de Salud Comunitaria'),
    #                                     ('4','Centro REHUE'),
    #                                     ('5','CESCO Freire'),
    #                                     ('6','CESCO Victoria'),
    #                                     ('7','Cesfam Bicentenario'),
    #                                     ('8','Ces++++++fam Dr. Hernán Urzúa Merino'),
    #                                     ('9','Cesfam Huamachuco'),
    #                                     ('10','Cesfam Renca'),
    #                                     ('11','Corporación Municipal de Renca'),
    #                                     ('12','COSAM Las Margaritas'),
    #                                     ('13','COSAM Los Plátanos'),
    #                                     ('14','COSAM Rucalaf'),
    #                                     ('15','Cumbres Condores Oriente'),
    #                                     ('16','Cumbres Condores Poniente'),
    #                                     ('17','Drogueria'),
    #                                     ('18','Escuela 1344 Lo Velázquez'),
    #                                     ('19','Escuela 1365 Gustavo Le Paige'),
    #                                     ('20','Escuela 314 Capitan Jose Luis Araneda'),
    #                                     ('21','Escuela 315 General Manuel Bulnes'),
    #                                     ('22','Escuela 316 Isabel Le Brun'),
    #                                     ('23','Escuela 317 Thomas Alva Edison'),
    #                                     ('24','Escuela 318 Domingo Santa Maria'),
    #                                     ('25','Escuela 325 Santa Juana de Lestonnac'),
    #                                     ('26','Escuela 326 Rebeca Matte'),
    #                                     ('27','Escuela 330 Juana Atala de Hirmas'),
    #                                     ('28','Escuela 332 Montserrat Robert'),
    #                                     ('29','Escuela 340 General Gorostiaga'),
    #                                     ('30','Farmacia Comunitaria'),
    #                                     ('31','Jardin Cumbre Cerro Aconcagua'),
    #                                     ('32','Jardin Cumbre Monte Everest'),
    #                                     ('33','Jardin Cumbre Monte Kilimanjaro'),
    #                                     ('34','Jardin Cumbre Torres del Paine'),
    #                                     ('35','Jardin Cumbre Volcán Lonquimay'),
    #                                     ('36','Jardin Cumbre Volcán Nevados de Incahuasi'),
    #                                     ('37','Jardin Cumbre Volcán Ojos Del Salado'),
    #                                     ('38','Jardin Cumbre Volcán Osorno'),
    #                                     ('39','Jardin Cumbre Volcán Parinacota'),
    #                                     ('40','Jardin Cumbre Volcán Villarica'),
    #                                     ('41','Optica Comunitaria'),
    #                                     ('42','SAPU Bicentenario'),
    #                                     ('43','SAPU Huamachuco'),
    #                                     ('44','SAR Bicentenario'),
    #                                     ('45','SAR Renca'),
    #                                     ('46','Otro')], track_visibility='onchange', index=True)

    # tipo_caso = fields.Selection([('1','Robo'),
    #                               ('2','Robo frustrado con detención'),
    #                               ('3','Robo frustrado sin detención'),
    #                               ('4','Robo interno'),
    #                               ('5','Agresión con lesión'),
    #                               ('6','Agresión sin lesión'),
    #                               ('7','Agresión verbal'),
    #                               ('8','Desordenes'),
    #                               ('9','Pendencia o riña int inst'),
    #                               ('10','Pendencia o riña externo inst'),
    #                               ('11','Procedimiento contorno instalación'),
    #                               ('12','Incidente interno instalación'),
    #                               ('13','Otro')
    #                               ], track_visibility='onchange', index=True)

    estado_caso = fields.Selection([('1','Abierto'),
                                  ('2','Cerrado')
                                  ], track_visibility='onchange', index=True)

    centro = fields.Many2one('centros.centros', string="Centro")
    type_caso = fields.Many2one('tipodecaso.tipodecaso', string="Tipo de Caso")
    folder_drive = fields.Char(string="URL Caso Google Drive", track_visibility='onchange', index=True)
    funcionarios_involucrados = fields.Text(string="Funcionarios Involucrados", track_visibility='onchange', index=True)
    descripcion_caso = fields.Text(string="Funcionarios Involucrados", track_visibility='onchange', index=True)
    usuario_responsable = fields.Many2one('res.users', track_visibility='onchange', index=True)
    message_attachment_count = fields.Integer(string="Conteo de archivos adjuntos", track_visibility='onchange', index=True)
    message_channel_ids = fields.Many2many('mail.channel', string="Seguidores (Canales)", track_visibility='onchange', index=True)
    message_follower_ids = fields.One2many('mail.followers', 'res_id', track_visibility='onchange', index=True)
    message_ids = fields.One2many('mail.message', 'res_id', track_visibility='onchange', index=True)
    message_main_attachment_id = fields.Many2one('ir.attachment', track_visibility='onchange', index=True)

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()

#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100

class centros(models.Model):
    _name = 'centros.centros'
   # _inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin']
    _description = "Centros de Costos"
    _rec_name = 'nombre_centro'
    nombre_centro = fields.Char(string="Nombre de Centro",track_visibility='onchange', index=True)
    direccion_centro = fields.Char(string="Dirección Centro",track_visibility='onchange', index=True)
    lat_centro = fields.Char(string="Latitud Centro",track_visibility='onchange', index=True)
    log_centro = fields.Char(string="Longitud Centro",track_visibility='onchange', index=True)




class tipo_de_caso(models.Model):
    _name = 'tipodecaso.tipodecaso'
    #_inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin']
    _description = "Tipo Casos Seguridad"
    _rec_name = 'nombre_tipo_caso'

    nombre_tipo_caso = fields.Char(string="Tipo de Caso",track_visibility='onchange', index=True)



@api.multi
@api.returns('mail.message', lambda value: value.id)
def message_post(self, **kwargs):
    if self.env.context.get('mark_rfq_as_sent'):
        self.filtered(lambda o: o.state == 'draft').write({'state': 'sent'})
    return super(PurchaseOrder, self.with_context(mail_post_autofollow=True)).message_post(**kwargs)

